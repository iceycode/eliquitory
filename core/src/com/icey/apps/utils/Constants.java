package com.icey.apps.utils;

import com.badlogic.gdx.utils.Array;
import com.icey.apps.data.Base;
import com.icey.apps.data.Flavor;

/** Constants class
 * - contains fields used throughout the
 *
 *
 * Created by Allen on 1/11/15.
 */
public class Constants {

    //--------global constants for app (used anywhere/everywhere)--------
    //default screen width & height
    public static final float SCREEN_HEIGHT = 800f;
    public static final float SCREEN_WIDTH = 480f;

    public static final float MIN_SCREEN_WIDTH = 800f/3;
    public static final float MIN_SCREEN_HEIGHT = 480f/3;

    //default values for advertisements
    public static final float AD_HEIGHT = 50;

    public static String SAVE_FILE = "saves/save.json" ;
    public static String SAVE_FILE_ENCODED = "saves/save_encoded.json";

    //Skin JSON file locations
    public static final String MENU_SKIN = "skins/menu/menuSkin.json"; //skin json file for menu
    public static final String SUPPLY_MENU_SKIN = "skins/supplies/supplySkin.json";
    public static final String CALC_SKIN = "skins/calculator/calcSkin.json"; //calc skin
    public static final String SETTING_SKIN = "skins/settings/settingSkin.json";

    //theme skins
    public static final String DARK_SKIN = "skins/dark/darkSkin.json";


    //names of settings
    public static final String SETTINGS_Desktop = "settings1";
    public static final String SETTINGS_Android = "settings2";
    public static final String SETTINGS_Web = "settings3";
    public static final String SETTINGS_iOS = "settings4";


    //=====================Menu Constants=====================-
    public static final float[] MENU_TITLE_SIZE = {275, 100};
    public static final float[] MENU_BTN_SIZE = {225, 100};



    //=====================Supply Constants=====================
    public static final String[] PERC_FIELD_NAMES = {"PG %: ", "VG %: ", "Other %: "}; //for percent textfield for base
    public static final String[] SUPPLY_CHECKBOX_TITLES = {"PG ", "VG ", "Other "};

    public static final String DELETE_CHECK = "Are you sure you want to delete?";

    public static final String[] SUPPLY_NAMES = {"Propylene Glycol", "Vegetable Glycerin",
            "Other: ", "Nicotine Base", "Flavor: "};

    public static final String[] OTHER_NAMES = {"Alcohol", "Water", "Other"};


    //=====================Calculator constants =====================
    public static final String GOAL_TITLE = " Desired Amount";
    public static final String BASE_TITLE = "Nicotine Base";
    public static final String FLAVORS_TITLE = "Flavor(s)";
    
    public static final String FINAL_CALCS_TITLE = "mL (drops)";
    public static final String SUPPLY_AMTS_TITLE = "Supply";
    public static final String[] AMOUNTS_TITLES = {"Amt (mL): ", "Str (mg): "}; //0=liq ml; 1 = str nicotine

    public static final String[] TYPE_NAMES = {"PG %: ", "VG %: ", "Other %: "};
    public static final String[] FLAV_TYPE_NAMES = {"PG", "VG", "Other"};
    public static final int[] GOAL_PERCENT_TYPES = {0, 1, 2, 5};
    public static final int[] BASE_PERCENT_TYPES = {3, 4, 6};

    public static final String NEW_FLAVOR_STRING = "New Flavor: ";
    public static Flavor NEW_FLAVOR = new Flavor("New Flavor");

    public static final String[] CALCLABEL_TEXT = {"PG: ", "VG: ", "Other: ", "Base: "};

    //error messages for when calulating
    public static final String ERROR_MAIN = "Error";
    public static final String[] ERROR_MSGS = {"A flavor type or percent not set", "Desired percents not at 100%", 
            "Base percents not at 100%", "Desired amount not set"};
    
    /**
     * 0-3 desired PG-->base vg; 4 flavor percent; 5-7 desired amount, desired strength, base strength
     */
    public static final String[] ERROR_DETAILS = {"DESIRED PG", "DESIRED VG",
            "BASE PG", "BASE VG %", "FLAVOR : ", "DESIRED AMOUNT"
            , "DESIRED STRENGTH", "BASE STRENGTH "};
    
    public static final int[] AMOUNT_LISTENER_TYPE = {0, 1}; //0=ml; 1 = mg (strength)

    //width & height of the text fields (percents)
    public static final float TEXT_FIELD_WIDTH = 50; //previously 150
    public static final float TEXT_FIELD_HEIGHT = 25f;


    public static final float TITLE_HEIGHT = 30f;
    public static final float TITLE_WIDTH = 200f;




    //----these are empty initial values---
    //default base strength & percents - user can change in settings
    public static final double ZERO_FINAL_AMOUNT = 0.0;
    public static Array<Integer> DEFAULT_DESIRED_PERCENTS = new Array<Integer>(new Integer[]{50, 50, 0});
    public static final double ZERO_STRENGTH = 0.0; //desired strength (medium)
    
    //base defaults - user will be able to change in settings
    public static final double EMPTY_BASE_STR = 100.0;
    public static Array<Integer> DEFAULT_BASE_PERCENTS = new Array<Integer>(new Integer[]{50, 50});
    public static Base DEFAULT_BASE = new Base(0, EMPTY_BASE_STR, DEFAULT_BASE_PERCENTS);

    //flavor default - the go-to flavor for user, can change in settings
    public static final  double DEFAULT_FLAV_AMT = 0; //amount of the flavor supply
    public static final String DEFAULT_FLAV_NAME = "Flavor1"; //name of flavor
    public static final Flavor EMPTY_FLAVOR = new Flavor(0, "Flavor1");

    public static Array<Double> INITLAL_FINAL_MLS = new Array<Double>(new Double[]{0.0, 0.0, 0.0, 0.0});

    //set to 20 as default value, but user can change this
    public static double DROPS_PER_ML = 20;


    public static class InfoText{
        public static final String ABOUT_LITE_V1 = "This is the first version of EJuice Toolkit Lite\n\n"+
                "A paid version will be coming shortly and this version will " +
                "\nlikely be updated soon after to include more features!\n" +
                "Also, graphics will change and multiple themes will be added\n";

        public static final String FEATURES_LITE_V1 = "Feature in lite version include:\n" +
                "- Setting drops per ml\n" +
                "- Basic calculations for Propylene Glycol, Vegetable Glycerin\n and Nicotine Base\n" +
                "- 'Other' amounts - this can be water, alcohol or other liquid\n added for consistency\n" +
                "- Ability to save and load recipes\n" +
                "- Ability to copy and paste calculated amounts\n";

        public static final String WARNING_V1 = "WARNING!\nDo not exceed 30mg nicotine content per EJuice Recipe.\n" +
                "Anything over even 24 mg is dangerously high.";
    }



    /** constants for tests
     *
     */
    public static class Tests {
        //=============Constants for CalcUtilTests (6 expected final amounts)============
        public static double DESIRED_AMT = 100.0;
        public static double DESIRED_STR = 10.0;
        public static Array<Integer> DESIRED_PERCS = new Array<Integer>(new Integer[]{30, 70, 0});
        public static Array<Integer> ALT_DESIRED_PERCS = new Array<Integer>(new Integer[]{30, 70, 10});

        public static Flavor FLAV1 = new Flavor("Flavor1", 10, 0);
        public static Flavor FLAV2 = new Flavor("Flavor2", 5, 1);
        public static Flavor FLAV3 = new Flavor("Flavor3", 5, 2);
        public static Array<Flavor> TEST_FLAVORS = new Array<Flavor>(new Flavor[]{FLAV1, FLAV2, FLAV3});

        public static Array<Integer> BASE_PERCS = new Array<Integer>(new Integer[]{50, 50});
        public static double BASE_STR = 100;
        public static Base TEST_BASE = new Base(BASE_STR, BASE_PERCS);


        /** 6 Total Final Amounts to Tests for ----based on 20 drops/ml
         * expect0 = PG/VG, 1 Flavor(PG); expect1 = PG/VG + 2 Flavors (PG, VG); expect2= PG/VG + 3 Flavors;
         *  expect3= PG/VG/Other + 1 Flavor(PG); expect4 = PG/VG/Other + 2 Flavors (PG, Other)
         *     expect5 = PG/VG/Other + 3 Flavors (PG, VG, Other)
         */
        public final static Double[][] EXPECTED_FAs = {{15.0, 65.0, 0.0, 10.0, 10.0},{15.0, 60.0, 0.0, 10.0, 10.0, 5.0},
                {13.5, 56.5, 0.0, 10.0, 10.0, 5.0, 5.0}, {12.0, 58.0, 10.0, 10.0, 10.0},
                {10.5, 54.5, 10.0, 10.0, 10.0, 5.0},{10.5, 49.5, 10.0, 10.0, 10.0, 5.0, 5.0}};

    }


    /** TODO: figure out how to (or if to) implement this class
     *  User defaults
     *  - will be able to be altered using the options menu
     */
    public static class UserDefaults{

        //default base strength & percents - user can change in settings
        public static double DEFAULT_FINAL_AMT = 30.0;
        public static Array<Integer> DEFAULT_DESIRED_PERCENTS = new Array<Integer>(new Integer[]{70, 30, 0});
        public static double DESIRED_STR = 16.0; //desired strength (medium)

        //base defaults - user will be able to change in settings
        public static double DEFAULT_BASE_STR = 100.0;
        public static Array<Integer> DEFAULT_BASE_PERCENTS = new Array<Integer>(new Integer[]{0, 0});
        public static Base DEFAULT_BASE = new Base(0, DEFAULT_BASE_STR, DEFAULT_BASE_PERCENTS);

        //flavor default - the go-to flavor for user, can change in settings
        public static double DEFAULT_FLAV_AMT = 0; //amount of the flavor supply
        public static String DEFAULT_FLAV_NAME = "Flavor1"; //name of flavor
        public static Flavor FLAVOR_DEFAULT = new Flavor(0, DEFAULT_FLAV_NAME);

        public static Array<Double> INITLAL_FINAL_MLS = new Array<Double>(new Double[]{0.0, 0.0, 0.0, 0.0, 0.0});
        
    }
    
}
